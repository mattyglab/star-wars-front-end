// https://docs.cypress.io/api/introduction/api.html

describe('All Characters Page', () => {
  it('Visits the app root url', () => {
    cy.visit('/');
    cy.get('.v-toolbar__title').find('.logo').should('exist');
  });

  it('Loads all characters page', () => {
    cy.contains('p.display-1', 'All Characters');
  });

  it('Displays list of at least 10 characters', () => {
    cy.get('.characters-list').find('.character-li').its('length').should('eq', 10);
  });

  it('Loads another page of characters on page bottom', () => {
    cy.scrollTo('bottom');
    cy.get('.characters-list').find('.character-li').should('have.length.gt', 10);
  });

  it('Clicking more info on a character loads their page', () => {
    cy.get('.characters-list').find('.character-li').first().find('.info-button').click();
    cy.location('hash').should('eq', '#/character/1');
  });
});


describe('Character Page', () => {
  it('Loads when visiting url', () => {
    cy.visit('/#/character/1');
    cy.contains('.character-headline', 'Luke Skywalker');
  });

  it('Displays species tag', () => {
    cy.get('.species-tag').find('.v-card__text').should('exist');
  });

  it('Displays homeworld tag', () => {
    cy.get('.homeworld-tag').find('.v-card__text').should('exist');
  });

  it('Displays attributes tag', () => {
    cy.get('.attributes-tag').find('.v-list-item').should('exist');
  });

  it('Displays vehicles list', () => {
    cy.get('.vehicles-list').find('.vehicle-li').should('have.length.gt', 1);
  });

  it('Displays starships list', () => {
    cy.get('.starships-list').find('.starship-li').should('have.length.gt', 1);
  });

  it('Displays films list', () => {
    cy.get('.films-list').find('.film-li').should('have.length.gt', 1);
  });

});