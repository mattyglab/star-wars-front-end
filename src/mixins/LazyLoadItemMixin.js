const LazyLoadItemMixin = {
  data() {
    return {
      loading: false,
      not_found: false,
      resource: '',
    };
  },
  props: {
    id: {
      type: String,
      required: true,
      validator(id) {
        return id.match(/^\d+$/) !== null;
      },
    },
  },
  mounted() {
    this.fetch();
  },
  updated() {
    this.fetch();
  },
  computed: {
    item() {
      return this.$store.state[`${this.resource}_map`][this.id];
    },
  },
  methods: {
    fetch() {
      if (this.item === undefined && !this.loading && !this.not_found) {
        this.loading = true;

        this.$store.dispatch('fetchItem', { resource: this.resource, id: this.id })
          .then(() => {
            this.loading = false;
          })
          .catch(() => {
            this.loading = false;
            this.not_found = true;
          });
      }
    },
  },
};

export default LazyLoadItemMixin;
