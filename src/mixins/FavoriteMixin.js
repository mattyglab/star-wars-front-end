const FavoriteMixin = {
  data() {
    return {
      resource: '',
    };
  },
  props: {
    id: {
      type: String,
      required: true,
      validator(id) {
        return id.match(/^\d+$/) !== null;
      },
    },
  },
  computed: {
    isFavorite() {
      return this.$store.state[`favorite_${this.resource}_ids`].indexOf(this.id) !== -1;
    },
  },
  methods: {
    toggleFavorite() {
      this.$store.dispatch('toggleFavorite', { resource: this.resource, id: this.id });
    },
  },
};

export default FavoriteMixin;
