import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist';
import SwapiApiService from './swapi.service';

Vue.use(Vuex);

const ResourceService = {
  people: new SwapiApiService('people'),
  films: new SwapiApiService('films'),
  species: new SwapiApiService('species'),
  planets: new SwapiApiService('planets'),
  vehicles: new SwapiApiService('vehicles'),
  starships: new SwapiApiService('starships'),
};

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
});

export default new Vuex.Store({
  plugins: [vuexLocal.plugin],
  state: {
    people_map: {},
    films_map: {},
    vehicles_map: {},
    planets_map: {},
    starships_map: {},
    species_map: {},


    people_count: 0,
    films_count: 0,
    vehicles_count: 0,
    planets_count: 0,
    starships_count: 0,
    species_count: 0,

    people_page: 1,
    films_page: 1,
    vehicles_page: 1,
    planets_page: 1,
    starships_page: 1,
    species_page: 1,

    favorite_people_ids: [],
    favorite_films_ids: [],
    favorite_vehicles_ids: [],
    favorite_planets_ids: [],
    favorite_starships_ids: [],
    favorite_species_ids: [],

  },
  getters: {
    // List of ids for resource
    idsList: state => resource => Object.keys(state[`${resource}_map`]),

    // Does resource have more items on server?
    moreAvailable: (state, getters) => (resource) => {
      const resourceCount = state[`${resource}_count`];
      return getters.idsList(resource).length < resourceCount || resourceCount === 0;
    },

    // Is resource list empty?
    emptyList: (state, getters) => resource => getters.idsList(resource).length === 0,

    // Is resource list less than than one full page?
    ltOnePage: (state, getters) => resource => getters.idsList(resource).length < 10,
  },
  mutations: {
    // Favorite item
    addFavorite(state, { resource, id }) {
      state[`favorite_${resource}_ids`].push(id);
    },

    // Unfavorite item
    removeFavorite(state, { resource, index }) {
      Vue.delete(state[`favorite_${resource}_ids`], index);
    },


    // Add single item
    addItem(state, { resource, item }) {
      Vue.set(state[`${resource}_map`], item.id, item);
    },

    // Add list of items
    addItemList(state, { resource, itemlist }) {
      itemlist.forEach((item) => {
        Vue.set(state[`${resource}_map`], item.id, item);
      });

      state[`${resource}_page`] += 1;
    },

    // Set total count for resource
    setCount(state, { resource, count }) {
      state[`${resource}_count`] = count;
    },
  },
  actions: {
    // add /remove favorite
    toggleFavorite({ commit, state }, { resource, id }) {
      const index = state[`favorite_${resource}_ids`].indexOf(id);

      if (index !== -1) {
        commit('removeFavorite', { resource, index });
      } else {
        commit('addFavorite', { resource, id });
      }
    },

    // Fetch single item from resource api
    fetchItem({ commit, state }, { resource, id }) {
      const resourceMap = state[`${resource}_map`];
      const resourceCount = state[`${resource}_count`];

      return new Promise(((resolve, reject) => {
        if (id in resourceMap) {
          resolve();
        } if (Object.keys(resourceMap).length < resourceCount || resourceCount === 0) {
          return ResourceService[resource].get(id)
            .then((response) => {
              commit('addItem', { resource, item: response.data });
              resolve();
            })
            .catch(reject);
        }
        reject();
      }));
    },

    // Fetch page of items from resource api
    fetchPage({ commit, state }, resource) {
      const resourceMap = state[`${resource}_map`];
      const resourceCount = state[`${resource}_count`];
      const resourcePage = state[`${resource}_page`];

      return new Promise(((resolve, reject) => {
        if (Object.keys(resourceMap).length < resourceCount || resourceCount === 0) {
          return ResourceService[resource].getPage(resourcePage)
            .then((response) => {
              commit('addItemList', { resource, itemlist: response.data.results });

              // Save total number of people if not saved
              if (resourceCount === 0) {
                commit('setCount', { resource, count: response.data.count });
              }
              resolve();
            })
            .catch(reject);
        }

        reject();
      }));
    },
  },
});
