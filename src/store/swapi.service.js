import axios from 'axios';

const baseURL = 'https://swapi.co/api';

class SwapiApiService {
  constructor(resource) {
    this.resourceURL = `${baseURL}/${resource}/`;

    this.apiClient = axios.create({
      baseURL: this.resourceURL,
    });

    const resourceURLRegex = `${baseURL}/(\\w+)/(\\d+)/`.replace(/[/]/g, '\\$&');
    this.idRegex = new RegExp(resourceURLRegex);
  }

  // Get a single item
  get(id) {
    return this.apiClient.get(`${id}`, {
      transformResponse: axios.defaults.transformResponse.concat(
        data => this.setIdOnResourceInstance(data),
      ),
    });
  }

  // Get a list of items by page number
  getPage(pageNumber = 1) {
    return this.apiClient.get('', {
      params: {
        page: pageNumber,
      },
      transformResponse: axios.defaults.transformResponse.concat(
        data => this.setIdOnResourceList(data),
      ),
    });
  }

  // Set id of all items in page results
  setIdOnResourceList(original) {
    const data = original;
    data.results = original.results.map(item => this.setIdOnResourceInstance(item));
    return data;
  }

  // Set ids of single item and related objects from url
  setIdOnResourceInstance(original) {
    const data = original;
    data.id = this.idRegex.exec(data.url)[2];

    if (data.homeworld) {
      data.homeworld_id = this.idRegex.exec(data.homeworld)[2];
    }

    if (data.films) {
      data.film_ids = data.films.map(
        url => this.idRegex.exec(url)[2],
      );
    }

    if (data.species) {
      data.species_ids = data.species.map(
        url => this.idRegex.exec(url)[2],
      );
    }

    if (data.vehicles) {
      data.vehicle_ids = data.vehicles.map(
        url => this.idRegex.exec(url)[2],
      );
    }

    if (data.starships) {
      data.starship_ids = data.starships.map(
        url => this.idRegex.exec(url)[2],
      );
    }

    if (data.people) {
      data.people_ids = data.people.map(
        url => this.idRegex.exec(url)[2],
      );
    }

    if (data.residents) {
      data.resident_ids = data.residents.map(
        url => this.idRegex.exec(url)[2],
      );
    }

    if (data.pilots) {
      data.pilot_ids = data.pilots.map(
        url => this.idRegex.exec(url)[2],
      );
    }

    return data;
  }
}

export default SwapiApiService;
