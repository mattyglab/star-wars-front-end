import Vue from 'vue';
import Router from 'vue-router';
import CharacterList from './views/CharacterList.vue';
import CharacterProfile from './views/CharacterProfile.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: CharacterList,
    },
    {
      path: '/character/:id/',
      name: 'character',
      component: CharacterProfile,
      props: true,
      // component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    },
  ],
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
});
